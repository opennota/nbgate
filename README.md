nbgate [![License](http://img.shields.io/:license-gpl3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0.html) [![Pipeline status](https://gitlab.com/opennota/nbgate/badges/master/pipeline.svg)](https://gitlab.com/opennota/nbgate/commits/master)
======

nbgate is a reverse proxy to [notabenoid.org](http://notabenoid.org). It provides access to the site under a single common account while preventing the account from being hijacked.

## Install

    go install gitlab.com/opennota/nbgate@latest

## Run

    nbgate -u username -p password
