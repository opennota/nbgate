module gitlab.com/opennota/nbgate

go 1.16

require (
	github.com/dchest/captcha v0.0.0-20200903113550-03f5f0333e1f
	github.com/gorilla/securecookie v1.1.1
)

// +heroku goVersion 1.16
