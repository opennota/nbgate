// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

// Reverse proxy to notabenoid.org
package main

import (
	"bytes"
	"encoding/base64"
	"errors"
	"flag"
	"fmt"
	"html"
	"io"
	"log"
	"net"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"os"
	"regexp"
	"strings"
	"sync"
	"time"

	"github.com/dchest/captcha"
	"github.com/gorilla/securecookie"
)

const (
	baseURL     = "http://notabenoid.org/"
	settingsURL = baseURL + "register/settings"
)

var (
	user       = flag.String("u", "", "Username")
	pass       = flag.String("p", "", "Password")
	addr       = flag.String("http", ":1337", "HTTP service address")
	heroku     = flag.Bool("heroku", false, "The app is running on Heroku")
	useCaptcha = flag.Bool("captcha", false, "Use captcha")

	loginMtx sync.Mutex
	jar, _   = cookiejar.New(nil)
	c        = http.Client{Jar: jar}

	rSensitivePath = regexp.MustCompile(`(?i)^/register|^/users/\d+/(edit|delete|upic|invites)`)

	secureCookie *securecookie.SecureCookie

	baseURLObj, _ = url.Parse(baseURL)
	host          = baseURLObj.Host
)

func copyHeader(dst, src http.Header) {
	for k, vs := range src {
		for _, v := range vs {
			dst.Add(k, v)
		}
	}
}

func logRequest(r *http.Request) {
	host, _, err := net.SplitHostPort(r.RemoteAddr)
	if err != nil {
		host = r.RemoteAddr
	}
	log.Println(host, r.Method, r.URL, r.Referer(), r.UserAgent())
}

var removeHeaders = []string{
	"Cookie",
	"Set-Cookie",
	"Connection",
	"Keep-Alive",

	"X-Forwarded-For",
	"X-Forwarded-Port",
	"X-Forwarded-Proto",
}

func send(req *http.Request) (*http.Response, error) {
	for _, cookie := range jar.Cookies(req.URL) {
		req.AddCookie(cookie)
	}
	resp, err := http.DefaultTransport.RoundTrip(req)
	if err != nil {
		return nil, err
	}
	if rc := resp.Cookies(); len(rc) > 0 {
		jar.SetCookies(req.URL, rc)
	}
	return resp, err
}

func getSessionCookie() *http.Cookie {
	for _, c := range jar.Cookies(baseURLObj) {
		if c.Name == "PHPSESSID" {
			return c
		}
	}
	return nil
}

var gaScriptTmpl = `<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=$GA_MEASUREMENT_ID"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', '$GA_MEASUREMENT_ID', {
    cookie_flags: 'SameSite=None;Secure',
    cookie_domain: '$GA_MEASUREMENT_DOMAIN'
  });
</script>`

var rSafePath = regexp.MustCompile(`^(?i)(/[-_a-z0-9]+)+/?$`)

func sanitizeReturnURL(returnURL string) string {
	u, err := url.Parse(returnURL)
	if err != nil {
		return "/"
	}
	if !rSafePath.MatchString(u.Path) {
		return "/"
	}
	result := u.Path
	if u.RawQuery != "" {
		result += "?" + u.Query().Encode()
	}
	return result
}

func captchaHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		id := captcha.New()
		w.Header().Add("Content-Type", "text/html")
		w.Header().Add("Cache-Control", "no-cache, no-store, must-revalidate")
		fmt.Fprintf(w, `<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Captcha</title>
  <style>
    body { display: flex; justify-content: center; }
    form { display: flex; flex-direction: column; }
    input[type="submit"] { align-self: center; margin-top: 10px; }
  </style>`)

		if os.Getenv("GA_MEASUREMENT_ID") != "" && os.Getenv("GA_MEASUREMENT_DOMAIN") != "" {
			fmt.Fprintf(w, os.ExpandEnv(gaScriptTmpl))
		}
		fmt.Fprintf(w, `</head>
<body>
  <form method="POST" action="/captcha">
    <img src="/captcha/%s.png">
    <input type="hidden" name="captcha_id" value="%[1]s"></input>
    <input type="text" name="solution" autofocus></input>
    <input type="submit" value="Solve"></input>
    <input type="hidden" name="return_url" value="%s"></input>
  </form>
</body>
</html>`, id, html.EscapeString(r.FormValue("url")))
		return
	}
	captchaID := r.FormValue("captcha_id")
	solution := r.FormValue("solution")
	returnURL := "/"
	if captcha.VerifyString(captchaID, solution) {
		if encoded, err := secureCookie.Encode("captcha", time.Now().Format(time.RFC3339)); err == nil {
			u, _ := url.Parse(r.Host)
			c := &http.Cookie{
				Name:     "captcha",
				Value:    encoded,
				Path:     "/",
				Domain:   u.Host,
				Expires:  time.Now().Add(365 * 86400 * time.Second),
				HttpOnly: true,
			}
			if r.Proto == "https" {
				c.Secure = true
			}
			http.SetCookie(w, c)

			returnURL = sanitizeReturnURL(r.PostFormValue("return_url"))
		}
	}
	http.Redirect(w, r, returnURL, http.StatusFound)
}

func reverseProxy(w http.ResponseWriter, req *http.Request) {
	logRequest(req)

	if *heroku && os.Getenv("HASH_KEY") == "" {
		genkeyHandler(w, req)
		return
	}

	if rSensitivePath.MatchString(req.URL.Path) {
		w.WriteHeader(http.StatusForbidden)
		return
	}

	if *useCaptcha {
		captchaCookie, _ := req.Cookie("captcha")
		if captchaCookie == nil || secureCookie.Decode("captcha", captchaCookie.Value, nil) != nil {
			http.Redirect(w, req, "/captcha?url="+url.QueryEscape(req.URL.String()), http.StatusTemporaryRedirect)
			return
		}
	}

	loginMtx.Lock()
	if getSessionCookie() == nil {
		if err := login(); err != nil {
			log.Printf("login error: %v", err)
			w.WriteHeader(http.StatusInternalServerError)
			loginMtx.Unlock()
			return
		}
	}
	loginMtx.Unlock()

	outReq := new(http.Request)
	outReq.Method = req.Method
	outReq.URL = &url.URL{
		Scheme:   baseURLObj.Scheme,
		Host:     host,
		Path:     req.URL.Path,
		RawQuery: req.URL.RawQuery,
	}
	outReq.Proto = "HTTP/1.1"
	outReq.ProtoMajor = 1
	outReq.ProtoMinor = 1
	outReq.Header = make(http.Header)
	outReq.Body = req.Body
	outReq.ContentLength = req.ContentLength
	outReq.Host = host

	for _, h := range removeHeaders {
		req.Header.Del(h)
	}
	copyHeader(outReq.Header, req.Header)
	outReq.Header.Set("Host", host)
	outReq.Header.Set("Referer", baseURL)
	outReq.Header.Set("Origin", baseURL)

	resp, err := send(outReq)
	if err != nil {
		log.Printf("proxy error: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	defer resp.Body.Close()

	for _, h := range removeHeaders {
		resp.Header.Del(h)
	}
	if loc := resp.Header.Get("Location"); loc != "" {
		if loc == baseURL {
			jar.SetCookies(baseURLObj, nil)
			http.Redirect(w, req, req.URL.String(), http.StatusFound)
			return
		}
		if u, err := url.Parse(loc); err == nil && u.Host == host {
			u.Scheme = req.URL.Scheme
			u.Host = req.Host
			resp.Header.Set("Location", u.String())
		}
	}
	copyHeader(w.Header(), resp.Header)
	buf := new(bytes.Buffer)
	if strings.HasPrefix(resp.Header.Get("Content-Type"), "text/html") {
		_, _ = buf.ReadFrom(io.LimitReader(resp.Body, 4096))
		if bytes.Contains(buf.Bytes(), []byte("<!DOCTYPE html>")) {
			if !bytes.Contains(buf.Bytes(), []byte(fmt.Sprintf("login: '%s'", *user))) {
				jar.SetCookies(baseURLObj, nil)
				http.Redirect(w, req, req.URL.String(), http.StatusFound)
				return
			}
			if os.Getenv("GA_MEASUREMENT_ID") != "" && os.Getenv("GA_MEASUREMENT_DOMAIN") != "" {
				modified := bytes.Replace(buf.Bytes(), []byte("</head>"), append(
					[]byte(os.ExpandEnv(gaScriptTmpl)),
					[]byte("</head>")...), 1)
				buf = bytes.NewBuffer(modified)
			}
		}
	}
	w.WriteHeader(resp.StatusCode)

	_, _ = io.Copy(w, io.MultiReader(buf, resp.Body))
}

func robotsHandler(w http.ResponseWriter, req *http.Request) {
	logRequest(req)
	_, _ = w.Write([]byte("User-agent: *\nDisallow: /\n"))
}

func genkeyHandler(w http.ResponseWriter, req *http.Request) {
	logRequest(req)
	w.Header().Add("Content-Type", "text/plain")
	_, _ = w.Write([]byte(fmt.Sprintf("HASH_KEY=%s\n\nYou might want to add this variable to the Heroku app config page (section Config Vars), along with USER and PASSWORD\n", b64RandomKey(32))))
}

func login() error {
	loginForm := url.Values{
		"login[login]": {*user},
		"login[pass]":  {*pass},
	}
	resp, err := c.PostForm(baseURL, loginForm)
	if err != nil {
		return err
	}
	resp.Body.Close()
	if resp.StatusCode != 200 {
		return errors.New(resp.Status)
	}

	resp, err = c.Get(settingsURL)
	if err != nil {
		return err
	}
	resp.Body.Close()
	if resp.StatusCode != 200 {
		return errors.New(resp.Status)
	}

	return nil
}

func b64RandomKey(length int) string {
	return base64.StdEncoding.EncodeToString(securecookie.GenerateRandomKey(length))
}

func main() {
	flag.Parse()
	if flag.Arg(0) == "genkey" {
		fmt.Println("HASH_KEY=" + b64RandomKey(32))
		os.Exit(0)
	}

	if *user == "" || *pass == "" {
		fmt.Println("Usage: nbgate -u username -p password")
		os.Exit(1)
	}

	var hashKey []byte
	var err error
	if hk := os.Getenv("HASH_KEY"); hk != "" {
		hashKey, err = base64.StdEncoding.DecodeString(hk)
		if err != nil {
			log.Fatal("failed to decode HASH_KEY")
		}
	} else {
		hashKey = securecookie.GenerateRandomKey(32)
	}
	secureCookie = securecookie.New(hashKey, nil)

	http.HandleFunc("/", reverseProxy)
	http.HandleFunc("/captcha", captchaHandler)
	http.Handle("/captcha/", captcha.Server(240, 80))
	http.HandleFunc("/genkey", genkeyHandler)
	http.HandleFunc("/robots.txt", robotsHandler)
	log.Println("listening on", *addr)
	log.Fatal(http.ListenAndServe(*addr, nil))
}
